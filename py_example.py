import requests  # https://pypi.python.org/pypi/requests
import hashlib, json

printler = 'http://test.printler.pro'     # printler!
my_secret = ''                            # your private key
url = '%s/api/calculate/' % printler      # url for run calc
api_key = ''                              # your public key

# create hash for authentication on printler as follows: 
# hash = sha256(pubic_key + private_key)

with open('json_data_examples/visiting_card.json') as f:
    data = json.load(f)

data_string = json.dumps(data, separators=(",", ":"))

api_hash = hashlib.sha256(''.join([data_string, my_secret])).hexdigest()

response = requests.get(url, params={
    'data': data_string,
    'hash': api_hash,
    'key': api_key,
})
try:
    response_text = str(response.text).decode('unicode_escape')
except UnicodeError:
    response_text = response.text

print 'status_code:', response.status_code
print 'response:', response_text

# python 2.7

