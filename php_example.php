<?php

$printler  = "http://www.printler.pro/";    // printler!
$my_secret = "my private key for printler"; // your private key
$api_key   = "my public key for prinler";   // your public key
$domain    = "http://my.domain.com";        // your site domain

$hash = hash('sha256', $api_key.$my_secret);

$data_json = readfile('json_data_examples/visiting_card.json');
$opts = array(
  'http' => array(
    'method' => "GET",
    'header' => "X_API_KEY: " . $api_key . "\r\n" .
                "X_API_HASH: " . $hash . "\r\n" .
                "X_API_DOMAIN: " . $domain . "\r\n"
  )
);

?>
